package cvut.fel.dbs.lib.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Date;

@Embeddable
public class ArticleID implements Serializable {
    @Column( nullable = false)
    protected String title;
    @Column( nullable = false)
    protected Date created;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated() {
        return created.toString();
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
