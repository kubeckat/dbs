package cvut.fel.dbs.lib.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.LinkedList;
import java.util.List;


@Entity
public class Editor {
    @Id
    String penname;

    String specialization;
    String name;

    @ManyToMany(mappedBy = "editors")
    private List<Articles> articles;

    public String getPenname() {
        return penname;
    }

    public void setPenname(String penname) {
        this.penname = penname;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public List<Articles> getArticles() {
        return articles;
    }

    public void setArticles(LinkedList<Articles> articles) {
        this.articles = articles;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Editor{" +
                "penname='" + penname + '\'' +
                ", specialization='" + specialization + '\'' +
                ", name='" + name + '\'' +
                ", articles=" + articles +
                '}';
    }
}
