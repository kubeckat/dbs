package cvut.fel.dbs.lib.model;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Articles {
    @EmbeddedId
    private ArticleID articleID;

    private String content;


    @ManyToMany
    List<Editor>editors;

    public List<Editor> getEditors() {
        return editors;
    }

    public ArticleID getArticleID(){
        return articleID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setEditors(LinkedList<Editor> editors) {
        this.editors = editors;
    }

    public void setArticleID(ArticleID articleID){
        this.articleID=articleID;
    }
}
