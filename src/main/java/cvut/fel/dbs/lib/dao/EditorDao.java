package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Editor;
import cvut.fel.dbs.lib.model.Person;

import javax.persistence.EntityManager;
import java.util.List;

public class EditorDao {
    private EntityManager em;

    public EditorDao() {
    }

    public EditorDao(EntityManager em) {
        this.em = em;
    }

    public List<Editor> findAll(){
        return em.createQuery("SELECT p FROM Editor p", Editor.class).getResultList();
    }

    public void create(Editor p){
        em.persist(p);
    }

    public Editor find(String penname){
        return em.find(Editor.class, penname);
    }

    public Editor merge(Editor p){
        return em.merge(p);
    }

    public void delete(Editor p){
        em.remove(p);
    }
}