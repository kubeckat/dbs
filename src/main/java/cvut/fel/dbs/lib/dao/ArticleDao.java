package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Articles;
import cvut.fel.dbs.lib.model.ArticleID;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;

public class ArticleDao {
    private EntityManager em;

    public ArticleDao() {
    }

    public ArticleDao(EntityManager em) {
        this.em = em;
    }

    public List<Articles> findAll(){
        return em.createQuery("SELECT p FROM Articles p", Articles.class).getResultList();
    }

    public void create(Articles p){
        em.persist(p);
    }

    public Articles find(String title, String date){
        ArticleID a = new ArticleID() ;
        a.setTitle(title);
        a.setCreated(Date.valueOf(date));
        return em.find(Articles.class, a);
    }

    public Articles merge(Articles p){
        return em.merge(p);
    }

    public void delete(Articles p){
        em.remove(p);
    }
}

