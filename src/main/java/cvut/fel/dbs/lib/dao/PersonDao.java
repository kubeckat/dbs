package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Person;

import javax.persistence.EntityManager;
import java.util.List;

public class PersonDao {
    private EntityManager em;

    public PersonDao() {
    }

    public PersonDao(EntityManager em) {
        this.em = em;
    }

    public List<Person> findAll(){
        return em.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    }

    public void create(Person p){
        em.persist(p);
    }

    public Person find(Long id){
        return em.find(Person.class, id);
    }

    public Person merge(Person p){
        return em.merge(p);
    }

    public void delete(Person p){
        em.remove(p);
    }
}
