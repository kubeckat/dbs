package cvut.fel.dbs.lib;

import cvut.fel.dbs.lib.dao.ArticleDao;
import cvut.fel.dbs.lib.dao.EditorDao;
import cvut.fel.dbs.lib.dao.PersonDao;
import cvut.fel.dbs.lib.model.Articles;
import cvut.fel.dbs.lib.model.ArticleID;
import cvut.fel.dbs.lib.model.Editor;
import cvut.fel.dbs.lib.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.sql.Date;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private EntityManagerFactory emf;
    private EntityManager em;
    private PersonDao personDao;
    private ArticleDao articleDao;
    private EditorDao editorDao;

    public void run(){
        setup();
        workWithDB();
        close();
    }


    public void setup(){
        // create emf
        emf = Persistence.createEntityManagerFactory("pu");
        // create entity manager
        em = emf.createEntityManager();
        // create Person dao
        personDao = new PersonDao(em);
        articleDao = new ArticleDao(em);
        editorDao = new EditorDao(em);

        try {

            em.getTransaction().begin();

            Query q3 = em.createQuery("DELETE FROM Person");
            Query q4 = em.createQuery("DELETE FROM Articles");
            Query q1 = em.createQuery("DELETE FROM Editor");
            Query q2 = em.createQuery("DELETE FROM Book");

            q1.executeUpdate();
            q2.executeUpdate();
            q3.executeUpdate();
            q4.executeUpdate();

            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        em.getTransaction().begin();
        Editor editor = new Editor();
        editor.setPenname("kulicka");
        editor.setSpecialization("rozhovory");
        editor.setName("ales");
        editorDao.create(editor);
        em.getTransaction().commit();
        em.getTransaction().begin();
        Editor editor2 = new Editor();
        editor2.setPenname("ruza");
        editor2.setSpecialization("sloupky");
        editor2.setName("ruzena");
        editorDao.create(editor2);
        em.getTransaction().commit();
        em.getTransaction().begin();
        Editor editor3 = new Editor();
        editor3.setPenname("meda");
        editor3.setSpecialization("zpravy");
        editor3.setName("michal");
        editorDao.create(editor3);
        em.getTransaction().commit();
        Articles articles = new Articles();
        ArticleID articleID = new ArticleID();
        articleID.setTitle("Dobry den");
        articleID.setCreated(Date.valueOf("2003-03-04"));
        articles.setContent("Zdravim");
        articles.setArticleID(articleID);
        em.getTransaction().begin();
        articleDao.create(articles);
        em.getTransaction().commit();

    }

    public void close(){
        em.close();
        emf.close();
    }

    public void workWithDB(){
        em.getTransaction().begin();
        List<Person> people = personDao.findAll();
        em.getTransaction().commit();
        em.getTransaction().begin();
        List<Editor> e = editorDao.findAll();
        em.getTransaction().commit();
        LOG.info("Listing all users {}... ", people.size());

        for(Person p : people){
            LOG.info(p.toString());
        }
        for(Editor p : e){
            LOG.info(p.toString());
        }

        LOG.info("Generating 10 users.");
        for (int i = 0; i < 10; i ++){
            Person p = new Person();
            p.setName("Name - " + System.currentTimeMillis());
            em.getTransaction().begin();
            personDao.create(p);
            em.getTransaction().commit();
        }

        LOG.info("END");
    }
    private String cut(String s){
        String[] cut  = s.split(" ");
        String ret = "";
        for (int i = 0; i < 4 && i<cut.length; i++) {
            ret= ret+" "+cut[i];
        }
        return ret;
    }
    public String[][] getArticleTable(){
        LinkedList<String[]> ret=new LinkedList<>();
        em.getTransaction().begin();
        List<Articles> articles = articleDao.findAll();
        em.getTransaction().commit();
        for (Articles rs:articles) {
            String[] a =new String[3];
            a[0] =rs.getArticleID().getTitle();
            a[1] =rs.getArticleID().getCreated();
            a[2] =cut(rs.getContent());
            ret.add(a);
        }
        String[][]r = new String[ret.size()][3];
        System.out.println(ret.size());
        for (int i = 0; i < ret.size(); i++) {
            r[i] = ret.get(i);
        }
        System.out.println(Arrays.deepToString(r));
        return r;
    }

    public String[][] getEditorTable(){
        LinkedList<String[]> ret=new LinkedList<>();
        List<Editor> editors = editorDao.findAll();
        for (Editor rs:editors) {
            String[] a =new String[3];
            a[0] =rs.getName();
            a[1] =rs.getPenname();
            a[2] =rs.getSpecialization();
            ret.add(a);
        }
        String[][]r = new String[ret.size()][3];
        for (int i = 0; i < ret.size(); i++) {
            r[i] = ret.get(i);
        }
        return r;
    }

    public String[][] getWriteTable(){
        LinkedList<String[]> ret=new LinkedList<>();
        List<Editor> editors = editorDao.findAll();
        for (Editor e:editors) {
            for (Articles rs:e.getArticles()) {
            String[] a =new String[3];
            a[0] =rs.getArticleID().getTitle();
            a[1] =rs.getArticleID().getCreated();
            a[2] =e.getPenname();
            ret.add(a);
            }
        }
        String[][]r = new String[ret.size()][3];
        for (int i = 0; i < ret.size(); i++) {
            r[i] = ret.get(i);
        }
        return r;
    }
    public LinkedList<String>getEditors(){
        LinkedList<String> ret=new LinkedList<>();
        List<Editor> editors = editorDao.findAll();
        for (Editor rs:editors) {
            ret.add(rs.getPenname());
        }
        return ret;
    }
    public LinkedList<String>getArticles(){
        LinkedList<String> ret=new LinkedList<>();
        em.getTransaction().begin();
        List<Articles> articles = articleDao.findAll();
        em.getTransaction().commit();
        for (Articles rs:articles) {
            ret.add(rs.getArticleID().getTitle()+" ("+rs.getArticleID().getCreated()+ ")");
        }
        return ret;
    }


    public String putArticle(String title, String date, String content){
        if(title.equals(""))return "Please fill title";
        if(date.equals(""))return "Please fill date";
        if(content.equals(""))return "Please fill content";
        if(!date.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]"))return "Date must be YYYY-MM-DD";

        Articles articles = new Articles();
        articles.setContent(content);
        ArticleID articleID = new ArticleID();
        articleID.setCreated(Date.valueOf(date));
        articleID.setTitle(title);
        articles.setArticleID(articleID);
        try {
            em.getTransaction().begin();
            articleDao.create(articles);
            em.getTransaction().commit();
        }catch (Exception e){
            return "Something went wrong... \ncheck if title or date are unique,\ncheck if content is unique";
        }
        return "";
    }
    public String putWrite(String title, String date, String penname){
        System.out.println(title+" "+date);
        em.getTransaction().begin();
        Articles articles = articleDao.find(title, date);
        em.getTransaction().commit();
        System.out.println(articles.toString());
        em.getTransaction().begin();
        Editor editor= editorDao.find(penname);
        em.getTransaction().commit();

        //articles.getEditors().add(editor);
        if(editor.getArticles().contains(articles))return "This pair already exists";
        editor.getArticles().add(articles);
            em.getTransaction().begin();
            //em.merge(articles);
            editorDao.merge(editor);
            em.getTransaction().commit();

        return "";
    }
    public String getContent(String title, String date){
        em.getTransaction().begin();
        Articles a =articleDao.find(title, date);
        em.getTransaction().commit();

        return a.getContent();
    }

   public String changeArt(String title, String date, String content){
        try {
            em.getTransaction().begin();
            Articles a = articleDao.find(title, date);
            em.getTransaction().commit();
            a.setContent(content);
            em.getTransaction().begin();
            articleDao.merge(a);
            em.getTransaction().commit();
        }catch (Exception e){
            return "problem occurred";
        }
        return "";
    }
    public String deleteArt(String title, String date){
        em.getTransaction().begin();
        List<Editor> editors = editorDao.findAll();
        em.getTransaction().commit();
        for (Editor e:editors) {
            deleteWrite(title, date, e.getPenname());
        }

        try {

            em.getTransaction().begin();
            Articles a = articleDao.find(title, date);
            em.getTransaction().commit();
            System.out.println(a.toString());
            em.getTransaction().begin();
           // em.createQuery("DELETE FROM Articles p " +
             //  "WHERE p.title = "+title)
               //  .executeUpdate();
em.remove(a);
            em.getTransaction().commit();
        }catch (Exception e){
            return "problem occurred";
        }
        return "";
    }
    public void deleteWrite(String title, String date, String penname){
        em.getTransaction().begin();
        Articles articles = articleDao.find(title, date);
        em.getTransaction().commit();
        System.out.println(articles.toString());
        em.getTransaction().begin();
        Editor editor= editorDao.find(penname);
        em.getTransaction().commit();

        //articles.getEditors().add(editor);
        if(!editor.getArticles().contains(articles))return ;
        editor.getArticles().remove(articles);
        em.getTransaction().begin();
        //em.merge(articles);
        editorDao.merge(editor);
        em.getTransaction().commit();

    }
}
