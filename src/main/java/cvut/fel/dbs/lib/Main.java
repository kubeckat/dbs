package cvut.fel.dbs.lib;

import cvut.fel.dbs.lib.show.Show;

public class Main {
    public static void main(String[] args) {
Application application = new Application();
application.setup();
        Show show = new Show(application);
    }
}
