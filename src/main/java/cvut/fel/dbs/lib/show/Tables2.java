package cvut.fel.dbs.lib.show;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.show.ui.Change;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;

public class Tables2 {
    private JPanel myPanel;
    private JTable table1;
    private JTable table2;
    private JTable table3;
    private JButton wiewChangeButton;
    private JButton button1;
    private JTextField titleTextField;
    private JTextField dateYYYYMMDDTextField;
    private JComboBox comboBox1;
    private JButton addEditorButton;
    private JComboBox comboBox2;
    private JTextArea textArea1;
    private JButton deleteRelationButton;
    private Application database;
    private Frame frame;

    public Tables2(Application database, Frame frame){
        this.database = database;
        this.frame = frame;
        createTable(table1,database.getArticleTable(), new String[]{"Title", "Date", "Content starts"});
        createTable(table2,database.getEditorTable(), new String[]{"Name", "Penname", "Specialization"});
        createTable(table3,database.getWriteTable(), new String[]{"Title", "Date", "penname"});
        dateYYYYMMDDTextField.setText(java.time.LocalDate.now().toString());
        prepareCombo();

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("clicked");
                whenClickedFirst();
            }
        });
        addEditorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("clicked");
                whenClickedSecond();
            }
        });
        wiewChangeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                whenClickedChange();
            }
        });
        deleteRelationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                whenClickedThird();
            }
        });
    }
    public JPanel getMyPanel() {
        return myPanel;
    }
    private void createTable(JTable tab, String[][] data, String[] columns){
        tab.setModel(new DefaultTableModel(data, columns));
        tab.setDefaultEditor(Object.class, null);
    }
    private void prepareCombo(){
        System.out.println("combo");
        LinkedList<String> val = database.getEditors();
        comboBox1.removeAllItems();
        for (String s:val) {
            comboBox1.addItem(s);
        }
        comboBox2.removeAllItems();
        val = database.getArticles();
        for (String s:val) {
            comboBox2.addItem(s);
        }
    }
    private void whenClickedFirst(){
        String title = titleTextField.getText();
        String date = dateYYYYMMDDTextField.getText();
        String content = textArea1.getText();
        String res = database.putArticle(title ,date, content);
        if(res.equals("")){
            createTable(table1, database.getArticleTable(),  new String[]{"Title", "Date", "Content starts"});
            comboBox2.addItem(title+" ("+date+")");
        }
        else showError(res);
    }
    private void whenClickedSecond(){
        String ret = comboBox2.getSelectedItem().toString();
        System.out.println(ret);
        String title = ret.split(" \\(")[0];
        String date = ret.split(" \\(")[1];
        date = date.split("\\)")[0];
        String penname = comboBox1.getSelectedItem().toString();
        System.out.println(title);
        System.out.println(date);
        String out = database.putWrite(title, date, penname);
        if(out.equals("")){
            System.out.println(Arrays.deepToString(database.getWriteTable()));
            createTable(table3,database.getWriteTable(), new String[]{"Title", "Date", "penname"});
        }else {
            showError(out);
        }
    }
    private void whenClickedThird(){
        int row = table3.getSelectedRow();
        String title = (String) table3.getModel().getValueAt(row, 0);
        String date = (String) table3.getModel().getValueAt(row, 1);
        String penname = (String) table3.getModel().getValueAt(row, 2);
        database.deleteWrite(title, date, penname);
        createTable(table3,database.getWriteTable(), new String[]{"Title", "Date", "penname"});
    }
    private void showError(String err){
        JOptionPane.showMessageDialog(frame, err);
    }
    private void whenClickedChange(){
        if(table1.getSelectedRow()==-1)showError("Select row please");
        else {
            String title = table1.getModel().getValueAt(table1.getSelectedRow(), 0).toString();
            String date = table1.getModel().getValueAt(table1.getSelectedRow(), 1).toString();
            JFrame myFrame = new JFrame();
            Change change = new Change(title, date, database, this);

            myFrame.setResizable(true);
            myFrame.setLocationRelativeTo(null);
            myFrame.setVisible(true);
            myFrame.setSize(600, 400);
            myFrame.add(change.getNajdime());
            //changeWindArticle(title, date);
            createTable(table1,database.getArticleTable(), new String[]{"Title", "Date", "Content starts"});
            createTable(table2,database.getEditorTable(), new String[]{"Name", "Penname", "Specialization"});
            createTable(table3,database.getWriteTable(), new String[]{"Title", "Date", "penname"});
        }
    }

    private void changeWindArticle(String ti, String da){
        JFrame changeFrame  = new JFrame();
        changeFrame.setSize(250, 100);
        Button Cancbutton = new Button("Cancel");
        Cancbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeFrame.dispose();
                return;
            }
        });
        Button Changebutton = new Button("Do change");
        Button DeleteButton = new Button("Delete");
        JTextField title = new JTextField();
        title.setText(ti);
        JTextField date = new JTextField();
        date.setText(da);
        JTextArea area = new JTextArea();
        area.append(database.getContent(ti, da));
        Changebutton.setBounds(110,200,  70, 50);
        DeleteButton.setBounds(10,200,  70, 50);
        Cancbutton.setBounds(210,200,  70, 50);
        area.setBounds(10, 80, 100, 100);
        changeFrame.setSize(700, 500);
        changeFrame.add(title);
        changeFrame.add(date);
        changeFrame.add(area);
        changeFrame.add(Cancbutton);
        changeFrame.add(Changebutton);
        changeFrame.add(DeleteButton);
        changeFrame.setVisible(true);
    }
    public void closeChange(){

        createTable(table1,database.getArticleTable(), new String[]{"Title", "Date", "Content starts"});
        createTable(table2,database.getEditorTable(), new String[]{"Name", "Penname", "Specialization"});
        createTable(table3,database.getWriteTable(), new String[]{"Title", "Date", "penname"});
        dateYYYYMMDDTextField.setText(java.time.LocalDate.now().toString());
        prepareCombo();

    }

}
