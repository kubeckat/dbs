package cvut.fel.dbs.lib.show;

import cvut.fel.dbs.lib.Application;


import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Show {
    JFrame myFrame;
    JPanel panel;
    //Database database;
    Application application;
    public Show(Application application) {
        this.application = application;
        myFrame = new JFrame();
        Tables2 ui = new Tables2(application, myFrame );
        panel = ui.getMyPanel();
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myFrame.setResizable(true);
        myFrame.setLocationRelativeTo(null);
        myFrame.setVisible(true);
        myFrame.setSize(800, 450);
        myFrame.add(panel);
        myFrame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                super.windowClosing(e);
                application.close();
            }
        });
    }


}
