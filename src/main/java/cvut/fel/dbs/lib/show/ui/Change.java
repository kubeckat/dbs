package cvut.fel.dbs.lib.show.ui;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.show.Tables2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Change {
    private JPanel najdime;
    private JTextField dateTextField;
    private JTextField titleTextField;
    private JTextArea textArea1;
    private JButton deleteButton;
    private JButton changeButton;
    private final String title;
    private final String date;
    private final Application database;
    public Change(String title, String date, Application database, Tables2 tables2){

        this.title = title;
        this.date = date;
        this.database = database;
        titleTextField.setText(title);
        dateTextField.setText(date);
        textArea1.append(database.getContent(title, date));
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                database.changeArt(title, date, textArea1.getText());
                tables2.closeChange();

            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                database.deleteArt(title, date);
                tables2.closeChange();
            }
        });
    }

    public JPanel getNajdime() {
        return najdime;
    }
}
